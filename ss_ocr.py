'''
Step #1: Localize the LCD on the thermostat. This can be done using edge detection since there is enough contrast between the plastic shell and the LCD.

Step #2: Extract the LCD. Given an input edge map I can find contours and look for outlines with a rectangular shape — the largest rectangular region should correspond to the LCD. 
A perspective transform will give me a nice extraction of the LCD.

Step #3: Modified for Pfeiffer pressure gauge module. Extract the ROIs for Large Digits (3) and exponent digits (2).
Apply thresholding and morphological operations.

Step #4: Define the seven segment ROIs for each digit ROI. Transform to black and white and apply binary thresholding.

Heavily modified and extended by Olli Ohls on 24.5.2020
Originally presented by Adrian Rosebrock in https://www.pyimagesearch.com/2017/02/13/recognizing-digits-with-opencv-and-python/
'''
import time
import imutils
import cv2
import time
import math
import csv
import os
import ast
import sys
import re
from os import listdir
from os.path import isfile, join
from imutils.perspective import four_point_transform
from imutils import contours
import frame_extractor as fe
import post_processing as pp

CV2_THRESHOLD = 127 #default value

'''******FOR DEBUG USE******'''
LARGE_DIGIT_BOUNDING_BOXES = False
EXPONENT_BOUNDING_BOXES = False
SEGMENTS_ON = False
SEGMENT_SCORES = False

'''*********BINARY THRESHOLDING THRESHOLD VALUES*********'''
LARGE_DIGIT_IDENTIFIER_THRESHOLD_ACCURACY = 0.3
EXPONENT_DIGIT_IDENTIFIER_THRESHOLD_ACCURACY = 0.25

'''********DIGIT LOOKUP DICTS**********'''

DIGITS_LOOKUP = {
	(1, 1, 1, 0, 1, 1, 1): 0,
	(0, 0, 1, 0, 0, 1, 0): 1,
	(1, 0, 1, 1, 1, 0, 1): 2,
	(1, 0, 1, 1, 0, 1, 1): 3,
	(0, 1, 1, 1, 0, 1, 0): 4,
	(1, 1, 0, 1, 0, 1, 1): 5,
	(1, 1, 0, 1, 1, 1, 1): 6,
	(1, 0, 1, 0, 0, 1, 0): 7,
	(1, 1, 1, 1, 1, 1, 1): 8,
	(1, 1, 1, 1, 0, 1, 1): 9,
    }

EXPONENT_DIGITS_LOOKUP = {
	(0, 0, 1, 0, 0, 1, 0): 1,
	(1, 0, 1, 1, 1, 0, 1): 2,
	(1, 0, 1, 1, 0, 1, 1): 3,
	(0, 1, 1, 1, 0, 1, 0): 4,	
    (1, 1, 0, 1, 1, 0, 1): 'E',
    (0, 0, 0, 0, 0, 0, 0): 'R'
}

'''********************UTILITY FUNCTIONS******************'''

def ocr_start(video_filename, frame_cap_interval_seconds, amount_of_calibration_frames):
    '''Runs through all the steps. For dummies.'''
    print("Auto-detecting video fps..")
    video_fps = autodetect_video_fps(video_filename)
    video_length = autodetect_video_length_seconds(video_filename, video_fps)

    print('Generating calibration frames from video file: '+video_filename)
    fe.generate_calib_frames_and_save_readings(video_filename, round(video_fps), video_length, amount_of_calibration_frames, 100, 'NO_ROTATION')
    print('Searching for optimal threshold value based on calibration data')    
    optimum_thresh = get_optimized_threshold('calibration', 100.0, 200.0, 1.0)
    print('Optimum threshold value: '+str(optimum_thresh))
    print('*'*35)
    print('Starting frame extraction in.. ')    
    new_folder_name = fe.extract_frames(video_filename, round(video_fps), frame_cap_interval_seconds, 100, 'NO_ROTATION')
    print('Frame extraction complete')
    print('Continuing to perform OCR on extracted frames..')
    run_ocr_without_comparison(new_folder_name, optimum_thresh)
    print('Process complete. ')
    print('Results folder: ', new_folder_name)
    pp.post_process(os.path.join(new_folder_name, 'OCR_results.csv'))

def autodetect_video_length_seconds(video_filename, video_fps):
    '''Return video length'''
    duration_seconds = 0
    try:
        video = cv2.VideoCapture(video_filename)
    except FileNotFoundError:
        raise FileNotFoundError
    
    # Find OpenCV version
    (major_ver, minor_ver, subminor_ver) = (cv2.__version__).split('.')
    
    if int(major_ver) < 3 :
        frame_count = int(video.get(cv2.cv.CAP_PROP_FRAME_COUNT))
        duration_seconds = frame_count / video_fps
        print("Video duration (seconds): {0}".format(duration_seconds))
    else :
        frame_count = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
        duration_seconds = frame_count / video_fps
        print("Video duration (seconds): {0}".format(duration_seconds))
    
    video.release()
    #round up to whole number
    return duration_seconds


def run_ocr_without_comparison(data_set_folder_name, cv2_threshold):
    '''run the ocr on a set of frames'''
    t_and_reading = {}
    global CV2_THRESHOLD
    path = os.path.join(os.getcwd(), data_set_folder_name)
    CV2_THRESHOLD = cv2_threshold
    for frame_name in get_list_of_filenames_in_dir(data_set_folder_name):
        digit_results = run_extractor(data_set_folder_name, frame_name)
        #remove if most significant digit is N
        #cleaned_up_result = remove_when_most_significant_digit_unidentified(digit_results)
        #if cleaned_up_result is not None:
        t_and_reading[frame_name[1:].replace('.jpg','')] = digit_results
    write_data_in_file(os.path.join(path, 'OCR_results.csv'), t_and_reading)

def remove_when_most_significant_digit_unidentified(digit_results):
    '''if first digit is N remove entry from results'''
    print('digit_result[0]: ', digit_results[0])
    if digit_results[0] == 'N':
        print("Found N on ", digit_results)
        return None          
    return digit_results

def normalize_results(digit_results):
    '''Run through results and remove values that are outliers'''
    print("here")

def run_ocr_comparison(data_set_folder_name: str, results_set: dict, identifier_threshold ,cv2_threshold):
    '''run the ocr on a set of frames and compare results with results_set data'''
    global CV2_THRESHOLD
    
    path = os.path.join(os.getcwd(), data_set_folder_name)
    
    CV2_THRESHOLD = cv2_threshold
    print('AT OCR COMPARISON WITH IDENTIFIER THRESHOLD : ',identifier_threshold,', AND cv2_threshold :', cv2_threshold)
    extracted_results = []
    for key,val in results_set.items():
        extracted_results.append(run_extractor(data_set_folder_name, key))
    
    accuracy_results = test_accuracy(extracted_results, results_set)
    total_res = 0
    for test, res in accuracy_results.items():
        print(test,' : ',res)
        total_res += res[0]
    #print('Test frames: ',len(accuracy_results))
    print('Total accuracy: ',(total_res/len(accuracy_results)))
    return (total_res/len(accuracy_results))

def run_extractor(data_set_folder_name: str, filename: str):
    '''runs extractor on filename'''
    
    img_path = os.path.join(os.getcwd(), data_set_folder_name, filename)
    #print("Loading image from path: ", img_path)
    
    image = cv2.imread(img_path)
    
    #pre process stage 1    
    edged_img, greyed_img, im_image = pre_process1(image)
    
    #contour stage
    displayCnt = find_contours(edged_img)
    
    #extract thermostat display aka biggest rectangle and apply perspective transform to it
    output, warped = extract_thermostat_display_and_apply_perspective_transform(im_image, greyed_img, displayCnt)
    
    d1_output, roi_list = get_big_digit_roi_pfeiffer(output)
    
    exp_bounded, exp_roi_list = get_exponent_digit_roi_pfeiffer(d1_output)
    
    large_digits = extract_large_digits(roi_list, output, False)  
     
    exponent_digits = extract_large_digits(exp_roi_list, output, True)
    
    return format_results(large_digits, exponent_digits)
    
def format_results(large_digits, exponent_digits):
    '''Appends result integers and exponents on string'''
    results = ''
    for item in large_digits:
        if item == None:
            results = results+'N'
        else:
            results = results+str(item)
    for ed in exponent_digits:
        if item == None:
            results = results+'N'
        else:
            results = results+str(ed)    
    return results

def get_optimized_threshold(data_set_folder_name, min_thresh, max_thresh, increment):
    '''Uses the calibration frames to run the test. 
    Return the threshold value that gave the best result accuracy.'''
    global LARGE_DIGIT_IDENTIFIER_THRESHOLD_ACCURACY, EXPONENT_DIGIT_IDENTIFIER_THRESHOLD_ACCURACY

    user_annotated_results = read_txt_into_dict('calib_results.txt')
    
    highest_accuracy_thresh = min_thresh
    highest_accuracy = 0.0
    highest_large_identifier_threshold = 0.05
    for identifier_threshold in frange(0.05, 0.95, 0.03):
        print('*' * 35)
        print('LARGE_DIGIT_IDENTIFIER_THRESHOLD_ACCURACY: ', identifier_threshold)
        print('*' * 35)
        LARGE_DIGIT_IDENTIFIER_THRESHOLD_ACCURACY = identifier_threshold
        for cv2_threshold in frange(min_thresh, max_thresh, increment):
            result = run_ocr_comparison(data_set_folder_name, user_annotated_results, identifier_threshold, cv2_threshold)
            if result > highest_accuracy:
                highest_accuracy = result
                highest_large_identifier_threshold = identifier_threshold
                highest_accuracy_thresh = cv2_threshold
    print('reached accuracy: {} with threshold: {} and large_digit_identifier_threshold {}'.format(
          str(highest_accuracy), str(highest_accuracy_thresh), str(highest_large_identifier_threshold)))
    LARGE_DIGIT_IDENTIFIER_THRESHOLD_ACCURACY = highest_large_identifier_threshold

    #Set exponent digit threshold accuracy to same as large digit identification threshold
    EXPONENT_DIGIT_IDENTIFIER_THRESHOLD_ACCURACY = LARGE_DIGIT_IDENTIFIER_THRESHOLD_ACCURACY
    return highest_accuracy_thresh

def frange(start, stop=None, step=None):
    '''float range function'''
    if stop == None:
        stop = start + 0.0
        start = 0.0
    if step == None:
        step = 1.0
    count = 0
    while True:
        temp = float(start + count * step)
        if temp >= stop:
            break
        yield temp
        count += 1

def get_list_of_filenames_in_dir(dir_name):
    
    filenames = [f for f in listdir(dir_name) if isfile(join(dir_name, f))]
    filenames.sort(key=lambda f: int(re.sub('\D', '', f)))
    return filenames

def write_data_in_file(savepath, readings):
  '''create file with final results'''
  headers = ['t_sec', 'value']
  try:
    with open(savepath, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        for d in readings.items():
            writer.writerow(d)        
  except IOError:
    print('IO error')

'''***************IMAGE PROCESSING FUNCTIONS****************'''

def extract_roi(img, roi):
    '''crops the roi, region of interest and returns cropped image'''
    imCrop = img[int(roi[0][1]):int(roi[1][1]), int(roi[0][0]):int(roi[1][0])]
    return imCrop

def draw_segments_on_image(digit_img):
    '''Define the width and height for the seven segments of the number'''
    (roiH, roiW, o) = digit_img.shape
    (dW, dH) = (int(roiW * 0.26), int(roiH*0.15))
    dHC = int(roiH * 0.10)

    segment_top = ((int(roiW*0.42),int(roiH*0.01)), (int(roiW*0.85), int(dH*0.9)))
    segment_top_left = ((int(roiW*0.17), int(roiH*0.11)), (int(dW*1.4),int(roiH*0.4)))
    segment_top_right = ((int(roiW*0.83), int(roiH*0.11)), (int(roiW*0.98), int(roiH*0.40)))
    segment_middle = ((int(roiW*0.30), (roiH // 2) - int(dHC*0.9)) , (int(roiW*0.72), int(roiH*0.55)))
    segment_lower_left = ((int(roiW*0.07), int(roiH*0.58)), (dW, int(roiH*0.87)))
    segment_lower_right = ((int(roiW*0.69), int(roiH*0.58)), (int(roiW*0.9), int(roiH*0.87)))
    segment_bottom = ((int(roiW*0.27), int(roiH*0.88)), (int(roiW*0.63), int(roiH*0.97)))

    cropped_segments = []

    cropped_segments.append(extract_roi(digit_img, segment_top))    
    cropped_segments.append(extract_roi(digit_img, segment_top_left))   
    cropped_segments.append(extract_roi(digit_img, segment_top_right))    
    cropped_segments.append(extract_roi(digit_img, segment_middle))    
    cropped_segments.append(extract_roi(digit_img, segment_lower_left))    
    cropped_segments.append(extract_roi(digit_img, segment_lower_right))    
    cropped_segments.append(extract_roi(digit_img, segment_bottom))
    
    ########### SEGMENTS_ON only for debug for drawing bounding box on segments ############
    if SEGMENTS_ON:
        #draw_segments_on_debug(digit_img, cropped_segments)
        
        cv2.rectangle(digit_img, segment_top[0],segment_top[1],(255,0,0),1)
        cv2.rectangle(digit_img, segment_top_left[0],segment_top_left[1],(255,0,0),1)
        cv2.rectangle(digit_img, segment_top_right[0],segment_top_right[1],(255,0,0),1)
        cv2.rectangle(digit_img, segment_middle[0],segment_middle[1],(255,0,0),1)
        cv2.rectangle(digit_img, segment_lower_left[0],segment_lower_left[1],(255,0,0),1)
        cv2.rectangle(digit_img, segment_lower_right[0],segment_lower_right[1],(255,0,0),1)
        cv2.rectangle(digit_img, segment_bottom[0],segment_bottom[1],(255,0,0),1)
        cv2.imshow("7 segments", digit_img)
        cv2.waitKey(0)
        
    return digit_img, cropped_segments

def draw_segments_on_debug(digit_img, cropped_segments):
    for segment in cropped_segments:
        cv2.rectangle(digit_img, segment[0], segment[1],(255,0,0),1)

def make_gray(cropped_segments):
    '''Takes a list of cropped segment images and return same list as greyscale'''
    cropped_segments_grey = []
    for s in cropped_segments:
        g = cv2.cvtColor(s, cv2.COLOR_BGR2GRAY)
        cropped_segments_grey.append(g)
    return cropped_segments_grey

def apply_binary_thresholding(greyed_segments):
    '''Applies binary thresholding to images in greyscale'''
    global CV2_THRESHOLD
    bnw_segments = []
    for g in greyed_segments:
        (thresh, blackAndWhiteImage) = cv2.threshold(g, CV2_THRESHOLD, 255, cv2.THRESH_BINARY)
        bnw_segments.append(blackAndWhiteImage)
    return bnw_segments

def assign_binary_state_for_segments(segment_vals, max_vals, threshold):
    '''Sets a 0 or 1 for each segment val based threshold '''
    binary_segment_vals = []
    for i in range(len(segment_vals)):
        if (segment_vals[i] / max_vals[i]) > threshold:
            binary_segment_vals.append(1)
        else:
            binary_segment_vals.append(0)
    return binary_segment_vals

def calculate_segment_pixel_amount(segment_list):
    '''gets a list of segments and returns list of pixel amounts for each segment'''
    seg_pixel_amount = []
    for i in range(len(segment_list)):
        height, width = segment_list[i].shape
        seg_pixel_amount.append(int(height*width))
    return seg_pixel_amount
    
def calculate_non_zero_in_segments(greyed_segments):
    '''Takes a list of cropped grey segments and calculates the value of thresholded pixels in each segment.
    Returns a list of values for each segment.'''
    segment_values = []
    for g in greyed_segments:
        total = cv2.countNonZero(g)
        segment_values.append(total)
    return segment_values
    
def identify_digit(binary_segment_list, is_exponent):
    '''Is passed a list of binarized segments and returns the digit'''
    global DIGITS_LOOKUP
    b = binary_segment_list
    key = b[0], b[1], b[2], b[3], b[4], b[5], b[6]
    #print('key is: ', key)
    if is_exponent:
        digit = EXPONENT_DIGITS_LOOKUP.get(key)
    else:
        digit = DIGITS_LOOKUP.get(key)
    return digit

def get_big_digit_roi_pfeiffer(imgcv):
    '''returns ROIs for large digits in pfeiffer display'''
    roi_list = []
    height, width, channels = imgcv.shape
    digit_spacing = width * 0.020
    
    dw = int(0.165 * width)
    dh = int(0.38 * height)

    x1 = int(width * 0.105)
    y1 = int(height * 0.31)
    start_point = (x1,y1)
    end_point = (x1+dw,y1+dh)
    roi_list.append([start_point, end_point])
       
    start_point2 = (int(end_point[0]+digit_spacing), y1)
    end_point2 = (int(start_point2[0]+dw), int(start_point2[1]+dh))
    roi_list.append([start_point2, end_point2])
       
    start_point3 = (int(end_point2[0]+int(digit_spacing*1.00)), y1)
    end_point3 = (int(start_point3[0]+dw), int(start_point3[1]+dh)) 
    roi_list.append([start_point3, end_point3])   
    
    #for debug, set global variable
    if LARGE_DIGIT_BOUNDING_BOXES:
        cv2.rectangle(imgcv, start_point,end_point,(0,255,0),1)
        cv2.rectangle(imgcv, start_point2,end_point2,(0,255,0),1)
        cv2.rectangle(imgcv, start_point3,end_point3,(0,255,0),1)
        cv2.imshow("large digit", imgcv)
        cv2.waitKey(0)

    return imgcv, roi_list

def get_exponent_digit_roi_pfeiffer(imgcv):
    '''returns ROIs for exponent digits in pfeiffer display'''

    exp_roi_list = []
    height, width, channels = imgcv.shape
    e_digit_spacing = width * 0.115
    
    dw = int(0.095 * width)
    dh = int(0.21 * height)
    x1 = int(width * 0.525)
    y1 = int(height * 0.05)
    start_point = (x1,y1)
    end_point = (x1+dw, y1+dh)
    exp_roi_list.append([start_point, end_point])
       
    start_point2 = (int(end_point[0]+e_digit_spacing), y1)
    end_point2 = (int(start_point2[0]+dw), int(start_point2[1]+dh))    
    exp_roi_list.append([start_point2, end_point2])

    #for debug, set global variable
    if EXPONENT_BOUNDING_BOXES:
         cv2.rectangle(imgcv, start_point, end_point, (0,255,0), 1)
         cv2.rectangle(imgcv, start_point2, end_point2, (0,255,0), 1)
         cv2.imshow("exponent", imgcv)
         cv2.waitKey(0)

    return imgcv, exp_roi_list

def drawBoundingBox(imgcv, digit_contours):
    '''Draws a bounding box on an image using digit_contours and returns the image'''
    for c in digit_contours:
        (x, y, w, h) = cv2.boundingRect(c)
        cv2.drawContours(imgcv, contours, -1, (0, 255, 0), 3)
        cv2.rectangle(imgcv, (x, y), (x+w, y+h), (0, 255, 0), 2)      
        
    return imgcv 

def pre_process1(img):
    
    # pre-process the image by resizing it, converting it to
    # graycale, blurring it, and computing an edge map
    #image = imutils.resize(img, height=800)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (5, 5), 0)
    edged = cv2.Canny(blurred, 50, 200, 255)
    
    return edged, gray, img

def find_contours(img):
    
    # find contours in the edge map, then sort them by their
    # size in descending order
    cnts = cv2.findContours(img.copy(), cv2.RETR_EXTERNAL,
        cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    cnts = sorted(cnts, key=cv2.contourArea, reverse=True)
    displayCnt = None
    # loop over the contours
    for c in cnts:
        # approximate the contour
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.02 * peri, True)
        # if the contour has four vertices, then we have found
        # the thermostat display
        if len(approx) == 4:
            displayCnt = approx
            break
    return displayCnt

def extract_thermostat_display_and_apply_perspective_transform(image, gray, displayCnt):
    '''Extracts thermostat display from image and apply perspective transform'''
    warped = four_point_transform(gray, displayCnt.reshape(4, 2))
    output = four_point_transform(image, displayCnt.reshape(4, 2))
    return output, warped

def extract_large_digits(roi_list, output, is_exponent):

    global LARGE_DIGIT_IDENTIFIER_THRESHOLD_ACCURACY
    segments = ['top', 'top-left', 'top-right', 'lower-left', 'middle', 'lower-right','bottom']
    digits = []
    for item in roi_list:
        
        cropped_image = extract_roi(output, item)
        
        edged, gray, image = pre_process1(cropped_image)
        
        #get segments for one digit
        seg1, cropped_segs = draw_segments_on_image(cropped_image)
        
        #make all cropped segs gray
        greyed_segs = make_gray(cropped_segs)
        
        #apply binary thresholding to greyed images
        bnw_segs = apply_binary_thresholding(greyed_segs)
        
        #calculate non zero pixel amounts
        segment_vals = calculate_non_zero_in_segments(bnw_segs)
        
        if SEGMENT_SCORES:
            for i in range(len(segment_vals)):
                height, width = greyed_segs[i].shape
                print(segments[i],' : ', segment_vals[i], ' / ',str(height * width))
            print('*'*35)
        #get max pixel amount for segments
        max_pixel_vals = calculate_segment_pixel_amount(bnw_segs)

        #transform segment vals to binary map 0 or 1 for each segment, use exponent or large digit threshold accordingly
        if is_exponent:
            segment_binary_val_list = assign_binary_state_for_segments(segment_vals, max_pixel_vals, LARGE_DIGIT_IDENTIFIER_THRESHOLD_ACCURACY)
        else:
            segment_binary_val_list = assign_binary_state_for_segments(segment_vals, max_pixel_vals, LARGE_DIGIT_IDENTIFIER_THRESHOLD_ACCURACY)

        #identify the digit
        digit = identify_digit(segment_binary_val_list, is_exponent)
        digits.append(digit)
        
    return digits

def test_accuracy(extracted_results, correct_results):
    '''run through test dataset and compare with correct results'''
    counter = 0
    test_results = dict()
    for key, val in correct_results.items():
        res = 0
        if len(extracted_results[counter]) > 5:            
            a,b,c,d,e = extracted_results[counter].replace('None', 'N')
        else:
            a,b,c,d,e = extracted_results[counter]
        f,g,h,i,j = val
        if a == f:
            res += 0.2
        if b == g:
            res += 0.2
        if c == h:
            res += 0.2
        #if d == i:#
        res += 0.2
        if e == j:
            res += 0.2
                
        test_results[key] = (res, (a+b+c+d+e), val)
        counter = counter + 1
    return test_results

def read_txt_into_dict(fname):
    '''Opens file and returns contents as dict'''
    try:
        file = open(fname, "r")
        contents = file.read()
        dictionary = ast.literal_eval(contents)
        file.close()
    except IOError:
        print('IO error')
    return dictionary

def autodetect_video_fps(filename):
    try:
        video = cv2.VideoCapture(filename)
    except FileNotFoundError:
        raise FileNotFoundError
    
    # Find OpenCV version
    (major_ver, minor_ver, subminor_ver) = (cv2.__version__).split('.')
    
    if int(major_ver) < 3 :
        fps = video.get(cv2.cv.CV_CAP_PROP_FPS)
        print("Frames per second using video.get(cv2.cv.CV_CAP_PROP_FPS): {0}".format(fps))
    else :
        fps = video.get(cv2.CAP_PROP_FPS)
        print("Frames per second using video.get(cv2.CAP_PROP_FPS) : {0}".format(fps))
    
    video.release()
    #round up to whole number
    return fps
   
def main():
    '''Parameters: video_filename, frame capture interval, amount of calibration frames'''
    ocr_start('pressure2.mp4', 1, 15)

if __name__=='__main__':
    main()    