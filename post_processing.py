"""Post processing funtions"""

import csv
import os
import glob

def post_process(filepath):
    '''post processing routine'''
    print("Starting post-processing..")
    ocr_results = read_into_dict(filepath)
    clean_data = remove_none(ocr_results)
    no_unidentified = remove_reading_if_contains_unidentified_digit(clean_data)
    computable = change_to_mathematical_notation(no_unidentified)    
    save_post_processed_to_file(filepath, computable)   
    #cleanup('calibration')

def read_into_dict(filepath):
    '''Reads OCR_results.csv into a dict for post processing'''
    t_and_pressure = dict()
    try:
        with open(filepath) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                t_and_pressure[row[0]] = row[1]
    except IOError:
        print("IoError")
        raise IOError
    except FileNotFoundError:
        print("FileNotFound")
        raise FileNotFoundError
    return t_and_pressure

def remove_none(ocr_results):
    '''Reads file and replaces None with N and 4th digit with e'''
    for key,val in ocr_results.items():         
        if "None" in val:
            n_val = val.replace('None', 'N')
            ocr_results[key] = n_val      
    return ocr_results

def remove_reading_if_contains_unidentified_digit(ocr_results):
    '''Removes entry if N in any of the three digits'''
    keys_to_remove = []
    for key,val in ocr_results.items():
        digits = val[:-2]        
        if 'N' in digits:
            print(digits)
            keys_to_remove.append(key)
    for key in keys_to_remove:
        ocr_results.pop(key)
    return ocr_results

def change_to_mathematical_notation(ocr_results):
    for key, val in ocr_results.items():
        new_val = val[:3] + 'e-' + val[3+1:]
        ocr_results[key] = new_val
    return ocr_results

def save_post_processed_to_file(savepath, ocr_results):
    path = savepath[:-4]+'_postprocessed.csv'
    try:
        with open(path, 'w', newline='') as csvfile:
            fieldnames = ['t', 'pressure']
            writer = csv.writer(csvfile)
            for key,val in ocr_results.items():
                writer.writerow([key, val])
        print("Saved post-processed results at filepath: ",path)
    except IOError:
        print("IO Error")
        raise IOError

def remove_outliers_if_above_previous_by_percentage(percentage_threshold):
    '''Compares previous value and current value. If current value is over a certain percentage of previous value, 
    value is removed from dataset.'''
    print("todo")

def cleanup(path):
    '''Remove calibration frames from calibration folder'''
    files = glob.glob(path)
    print(files)
    for f in files:
        os.remove(f)

def main():
    '''main'''
    post_process(os.path.join('turppa', 'OCR_results.csv'))
    #cleanup(os.path.join('calibration'))
    
if __name__=='__main__':
    main()