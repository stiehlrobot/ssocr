import cv2
import numpy as np
import os
import random
import csv


def generate_calib_frames_and_save_readings(filename, video_fps, amount_of_frames, image_scale_percent, rotation):
  '''generates the calibration frames and prompts user to annotate readings'''
  print('HERE: generate_calib_frames_and_save_readings')
  #extract randomized calibration frames
  extracted_frame_names = extract_calibration_frames(filename, video_fps, amount_of_frames, image_scale_percent, rotation)

  #prompt user for valid digit identification for extracted frames
  correct_readings = annotate_calibration_frames(extracted_frame_names)
  
  write_calib_data_to_file('calib_results.txt', correct_readings)

def extract_calibration_frames(filename, video_fps, video_length_seconds, amount_of_frames, scale_percent, rotate_direction):
  '''Extracts and saves randomly picked frames and saves them in folder calibration'''
  print('HERE: extract_calibration_frames')
  extracted_frame_names = []
  cap = cv2.VideoCapture(filename)
  amount_of_frames = amount_of_frames
  
  # Check if camera opened successfully
  if (cap.isOpened() == False): 
    print("Error opening file. Check file exists with filename: ", filename)
  frame_counter = 0
  # Read until video is completed
  while(cap.isOpened()) and amount_of_frames > 0:
    
    # Capture frame-by-frame
    # randomize screen capture interval    
    
    screen_cap_interval_seconds = random.randint(10, 20)
    ret, frame = cap.read()
   
    if ret == True:
      frame_counter += 1
      if frame_counter % (video_fps*screen_cap_interval_seconds) == 0:
          amount_of_frames = amount_of_frames - 1
          
          resized = resize_frame(frame, scale_percent)              
          rotated = rotate_image(resized, rotate_direction)          
          cv2.imshow('Frame',rotated)
          
          extracted_frame_names = save_image_to_file('calibration', int(frame_counter/video_fps), rotated, extracted_frame_names)
          
      # Press Q on keyboard to  exit
      if cv2.waitKey(25) & 0xFF == ord('q'):
        break
    else: 
      break  
  cap.release()  
  cv2.destroyAllWindows()
  
  #returns list of extracted frames names
  return extracted_frame_names

def save_image_to_file(savepath, framenum, image, extracted_frame_names=None):
  '''Save frame with framenumber in name'''
  try:
    cv2.imwrite(os.path.join(savepath, "f{}.jpg".format(framenum)), image)
    if extracted_frame_names is not None:
      extracted_frame_names.append("f{}.jpg".format(framenum))
  except IOError as e:
      print(e)
  return extracted_frame_names

def resize_frame(frame, scale_percentage):
  '''Resizes and returns the frame given'''
  print('Original frame dimensions : ',frame.shape)
  '''***************************RESIZE FRAMES*******************************'''
  width = int(frame.shape[1] * scale_percentage / 100)
  height = int(frame.shape[0] * scale_percentage / 100)
  dimensions = (width, height)
  # resize image
  resized = cv2.resize(frame, dimensions, interpolation = cv2.INTER_AREA)
  print('Resized frame to : ',resized.shape)
  return resized
  

def rotate_image(image, direction):
  '''Uses open cv2.rotate to rotate image based on direction and degrees given
  Options for direction input are CW90, CCW90 and 180.
  '''
  if direction == 'CW_90':
    return cv2.rotate(image, cv2.ROTATE_90_CLOCKWISE)
  elif direction == 'CCW_90':
    return cv2.rotate(image, cv2.ROTATE_90_COUNTERCLOCKWISE)
  elif direction == '180':
    return cv2.rotate(image, cv2.ROTATE_180)
  elif direction == 'NO_ROTATION':
    return image
  else:
    print('Rotation parameters not recognized. Should be CW90, CCW90 or 180')
    raise

def annotate_calibration_frames(frame_list):
  '''shows the calibration frames to the user to fill in the correct value'''
  if len(frame_list) == 0:
    print("No extracted frames to annotate. ")
    raise
  else:
    user_annotated_readings = {}
    for frame in frame_list:  
      digits = prompt_for_digits(frame)
      user_annotated_readings[frame] = digits

  return user_annotated_readings

def create_folder(name_of_folder):
  '''created a folder in root'''
  if not os.path.isdir(name_of_folder):
    print(name_of_folder, "is not a dir.")
    os.mkdir(name_of_folder)
    return name_of_folder
  else:
    name_of_folder = input('Folder already exists. Specify new folder name: ')
    while os.path.isdir(name_of_folder):
      name_of_folder = input('Folder already exists. Specify new folder name: ')
    os.mkdir(name_of_folder)
    return name_of_folder

def extract_frames(filename, video_fps, screen_cap_interval_seconds, scale_percent, rotation_direction):
  '''Create a VideoCapture object and read from input file.'''
  cap = cv2.VideoCapture(filename)
  #create folder for extracted frames
  new_folder_name = create_folder(filename[:-4]+"_frames")
  extraction_path = os.path.join(os.getcwd(), new_folder_name)
  if (cap.isOpened()== False): 
    print("Error opening video stream or file")
  frame_counter = 0
  # Read until video is completed
  while(cap.isOpened()):
    # Capture frame-by-frame
    ret, frame = cap.read()
    if ret == True:
      frame_counter += 1
      print(frame_counter)
      if frame_counter % (video_fps*screen_cap_interval_seconds) == 0:
          print('Extracting frame', frame_counter)    
          resized = resize_frame(frame, scale_percent)            
          rotated = rotate_image(resized, rotation_direction)          
          cv2.imshow('Frame',rotated)
          
          save_image_to_file(extraction_path, int(frame_counter/video_fps), rotated)
          
      # Press Q on keyboard to  exit
      if cv2.waitKey(25) & 0xFF == ord('q'):
        break
    else: 
      break
  cap.release()
  cv2.destroyAllWindows()
  return new_folder_name

def prompt_for_digits(frame):
  '''prompts for digits and returns string of digits'''
  digits = input('specify digits in frame '+frame+' eg. 610E3: ').strip().upper()
  while len(digits) != 5:
    digits = input('specify digits in frame '+frame+' eg. 610E3: ').strip().upper()
    
  return digits

def write_calib_data_to_file(fname, readings):
  try:
    with open(fname,"w") as f:
      print(readings, file=f)
      f.close()
      print('wrote calib data to: ', fname)
  except IOError:
    print('IO error')

def generate_calib_frames_and_save_readings(filename, video_fps, video_length_seconds ,amount_of_frames, image_scale_percent, rotation):
  '''generates the calibration frames and prompts user to annotate readings'''
  print('HERE: generate_calib_frames_and_save_readings')
  #extract randomized calibration frames
  frame_names = extract_calibration_frames(filename, video_fps, video_length_seconds,amount_of_frames, image_scale_percent, rotation)

  #prompt user for valid digit identification as reference for calibration
  correct_readings = annotate_calibration_frames(frame_names)
  write_calib_data_to_file('calib_results.txt', correct_readings)

def main():  
  '''filename, video_fps, amount_of_frames, image_scale_percent, rotation'''
  #generate_calib_frames_and_save_readings('pressure2.mp4', 30, 10, 100, "NO_ROTATION")
  '''filename, extraction_folder_name, video_fps, screen_cap_interval_seconds, scale_percent, rotation_direction'''
  extract_frames('pressure2.mp4', "e_frames", 30, 10, 100, "NO_ROTATION")
  

if __name__=='__main__':
  main()